function updatePrice() {
  let s = document.getElementsByName("prodType");
  let amountOfGoods = document.getElementById("amountOfGoods").value;
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }

  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = (select.value == "2" ? "block" : "none");
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function (radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined && select.value == "2") {
        price += optionPrice;
      }
    }
  });


  let checkDiv = document.getElementById("checkboxes");
  checkDiv.style.display = (select.value == "3" ? "block" : "none");
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function (checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined && select.value == "3") {
        price += propPrice;
      }
    }
  });


  let prodPrice = document.getElementById("prodPrice");
  prodPrice.innerHTML = price * amountOfGoods + " p";
}

function getPrices() {
  return {
    prodTypes: [500, 500, 1000],
    prodOptions: {
      option1: 200,
      option2: 300,
      option3: 0,
    },
    prodProperties: {
      prop1: 400,
      prop2: 300,
    }
  }
}
window.addEventListener("DOMContentLoaded", function (event) {
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  let number = document.getElementById("amountOfGoods");
  number.addEventListener("change", function (event) {
    updatePrice();
  });
  let s = document.getElementsByName("prodType");
  let select = s[0];
  select.addEventListener("change", function (event) {
    updatePrice();
  });
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function (radio) {
    radio.addEventListener("change", function (event) {
      updatePrice();
    });
  });
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function (checkbox) {
    checkbox.addEventListener("change", function (event) {
      updatePrice();
    });
  });

  updatePrice();
});

function onClick() {
  alert("РЎРїР°СЃРёР±Рѕ Р·Р° РїРѕРєСѓРїРєСѓ");
}

window.addEventListener("DOMContentLoaded", function (event) {
  let b = document.getElementById("my-button");
  b.addEventListener("click", onClick);
});
